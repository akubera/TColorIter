==========
TColorIter
==========

A class for iterating over the TColorWheel


Installation
------------

Simply copy the ``TColorIter.h`` file into your project and include
in a source file.


Usage
-----

This class behaves similarly to a "standard" c++ iterator class, using
the dereference operator (``*``) to return a color, and increment
operators (``++``) to move the iterator to the next value.

The iterator should be given a list of colors over wich to increment;
upon incrementing past the end of this list, it will loop around, using
an "offset" of the original value.


.. code:: cpp

    TColorIter color({kRed, kGreen, kBlue});

    *color++; // kRed
    *color++; // kGreen
    *color++; // kBlue
    *color++; // kRed + 1
    *color++; // kGreen + 1


More functionality coming soon!
