///
/// \file TColorIter.h
/// \author Andrew Kubera, Ohio State University, andrew.kubera@cern.ch
///

#pragma once

#ifndef TCOLORITER_H_
#define TCOLORITER_H_

#include <TColor.h>
#include <TColorWheel.h>

#include <vector>

/// \class TColorIter
/// \brief Objects for easily iterating around the color wheel
///
class TColorIter {
protected:
  std::vector<int> fColors;
  size_t fIndex;
  size_t fOffest;
  size_t fStepSize;

public:

  static TColorIter GreenIter() {
    return TColorIter({kGreen});
  }

  static TColorIter RedIter() {
    return TColorIter({kRed});
  }

  TColorIter(std::vector<int> colors)
  : fColors(colors)
  , fIndex(0)
  , fOffest(0)
  , fStepSize(1)
  {}

  TColorIter(const TColorIter &orig)
  : fColors(orig.fColors)
  , fIndex(orig.fIndex)
  , fOffest(orig.fOffest)
  , fStepSize(orig.fStepSize)
  {}

  TColorIter operator++(int)
  {
    TColorIter copy(*this);
    ++(*this);
    return copy;
  }

  TColorIter& operator++()
  {
    ++fIndex;
    if (fIndex == fColors.size()) {
      fOffest += fStepSize;
      fIndex = 0;
    }
    return *this;
  }

  Int_t operator*() {
    return fColors[fIndex] + fOffest;
  }

  void SetStepsize(Int_t stepsize) {
    if (std::abs(stepsize) > 9) {
      std::cerr << "Warning - stepsize |" << stepsize << "| > 9, color values will likely pass "
                   "outside of expected color range";
    }
    fStepSize = stepsize;
  }

};

#endif
